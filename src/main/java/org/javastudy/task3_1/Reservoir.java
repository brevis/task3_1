package org.javastudy.task3_1;

public class Reservoir {
    double waterAmount;
    double minWaterAmount;
    double maxWaterAmount;
    double consumption;

    public Reservoir(double waterAmount, double minWaterAmount, double maxWaterAmount, double consumption) {
        this.waterAmount = waterAmount;
        this.minWaterAmount = minWaterAmount;
        this.maxWaterAmount = maxWaterAmount;
        this.consumption = consumption;
    }

    public double getConsumption() {
        return consumption;
    }

    public double getCurrentWaterAmount() {
        return waterAmount;
    }

    public double getFreeSpace() {
        return maxWaterAmount - waterAmount;
    }

    public void fill(double waterAmount) throws ReservoirFullException {
        double freeSpace = getFreeSpace();
        if (waterAmount > freeSpace) {
            waterAmount = freeSpace;
            this.waterAmount += waterAmount;
            throw new ReservoirFullException();
        } else {
            this.waterAmount += waterAmount;
        }
    }

    public double drain(double seconds) {
        if (waterAmount <= minWaterAmount) return 0;
        double amount = consumption / 3600 * seconds;
        if (amount > waterAmount - minWaterAmount) amount = waterAmount - minWaterAmount;
        this.waterAmount -= amount;
        return amount;
    }

}
