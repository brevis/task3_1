package org.javastudy.task3_1;

public class Sprinkler {

    private Reservoir reservoir;
    private Well well;
    private Pump pump;
    private double workingHours = 0;

    public void setReservoir(Reservoir reservoir) {
        this.reservoir = reservoir;
    }

    public void setWell(Well well) {
        this.well = well;
    }

    public void setPump(Pump pump) {
        this.pump = pump;
    }

    public void setWorkingHours(double hours) {
        this.workingHours = hours;
    }

    public double calculateWaterDeficit(boolean debug) {
        double waterNeeded = reservoir.getConsumption() * workingHours;
        double waterWatering = 0;

        int currentSecond = 0;
        int iterationTime = 1; // seconds
        int pumpOffTime = -(pump.getSleepTime() * 60 + 1);
        while(currentSecond < workingHours * 3600) {
            // pump water (well -> reservoir)
            if (currentSecond - pumpOffTime > pump.getSleepTime() * 60) {
                try {
                    pump.pump(iterationTime); // pump during `iterationTime` second(s)
                } catch (ReservoirFullException e) {
                    pumpOffTime = currentSecond;
                }
            }

            // watering (reservoir -> ...)
            waterWatering += reservoir.drain(iterationTime); // watering during `iterationTime` second(s)

            // refill well (... -> well)
            well.refill(iterationTime); // fill well during `iterationTime` second(s)

            currentSecond += iterationTime;

            // debug
            if (debug && currentSecond % 3600 == 0) {
                System.out.println(currentSecond/3600 + " hour:");
                System.out.println("Water in reservoir: " + reservoir.getCurrentWaterAmount());
                System.out.println("Water in well: " + well.getCurrentWaterAmount());
                System.out.println("Watering: " + waterWatering);
                System.out.println();
            }
        }

        return waterNeeded - waterWatering;
    }

    public static void main(String[] args) {
        double wellWaterAmount = 2800; // liter
        double wellFillSpeed = 300; // liter per hour
        Well well = new Well(wellWaterAmount, wellFillSpeed);

        double reservoirWaterAmount = 100; // liter
        double reservoirMinAmount = 100; // liter
        double reservoirMaxAmount = 900; // liter
        double reservoirConsumption = 210; // liter per hour
        Reservoir reservoir = new Reservoir(reservoirWaterAmount, reservoirMinAmount, reservoirMaxAmount, reservoirConsumption);

        double pumpPower = 5000; // liter per hour
        int pumpSleepTime = 30; // minutes
        Pump pump = new Pump(pumpPower, pumpSleepTime, well, reservoir);

        Sprinkler sprinkler = new Sprinkler();
        sprinkler.setPump(pump);
        sprinkler.setReservoir(reservoir);
        sprinkler.setWell(well);
        sprinkler.setWorkingHours(9);

        boolean debug = true;
        double waterDeficit = sprinkler.calculateWaterDeficit(debug);
        if (waterDeficit > 0) {
            System.out.printf("Water deficit: %.10f liter", waterDeficit);
        } else {
            System.out.printf("Water extra: %.10f liter", -waterDeficit);
        }
    }

}
