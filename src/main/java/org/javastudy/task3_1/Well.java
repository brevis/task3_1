package org.javastudy.task3_1;

public class Well {
    double waterAmount;
    double initWaterAmount;
    double fillSpeed;

    public Well(double waterAmount, double fillSpeed) {
        this.waterAmount = waterAmount;
        this.initWaterAmount = waterAmount;
        this.fillSpeed = fillSpeed;
    }

    public double refill(double seconds) {
        double amount = fillSpeed / 3600 * seconds;
        if (amount + waterAmount > initWaterAmount) {
            amount = initWaterAmount - waterAmount;
        }
        this.waterAmount += amount;
        return amount;
    }

    public double getWater(double amount) {
        if (amount > waterAmount) amount = waterAmount;
        waterAmount -= amount;
        return amount;
    }

    public double getCurrentWaterAmount() {
        return waterAmount;
    }

}
