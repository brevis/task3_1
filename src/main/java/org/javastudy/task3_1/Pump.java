package org.javastudy.task3_1;

public class Pump {
    double power;
    int sleepTime;
    Well well;
    Reservoir reservoir;

    public Pump(double power, int sleepTime, Well well, Reservoir reservoir) {
        this.power = power;
        this.sleepTime = sleepTime;
        this.well = well;
        this.reservoir = reservoir;
    }

    public int getSleepTime() {
        return sleepTime;
    }

    public void pump(double seconds) throws ReservoirFullException {
        //double reservoirFreeSpace = reservoir.getFreeSpace();
        //if (amount > reservoirFreeSpace) amount = reservoirFreeSpace;
        double amount = power / 3600 * seconds;
        double waterFromWell = well.getWater(amount);
        reservoir.fill(waterFromWell);
    }


}
